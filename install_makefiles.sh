#! /bin/bash
set -e  # Exit on errors
set -o pipefail
set -o nounset

# install_makefiles.sh

# add normal_Makefile to all directories 
for each_dir in data/*; do
    cp ./normal_makefile $each_dir/Makefile;
done

# If a folder contains a data file where the light and dark bits are switched
# (e.g., light values are 1 and dark values are 2), list the folder name
# below. 
#
# Add new folder names (or substrings of folder names) on lines below 'day_1'
special_data=( \
    "day_1" \
    )
            
for special_folder in "${special_data[@]}"
do
    cp ./alternate_makefile "data/"$(ls data/ | grep "$special_folder")/Makefile
done

# If there's a folder in `inverted_data` that doesn't exist in the data
# directory, we'll have a misplaced Makefile - we remove it here
rm -f "data/Makefile"
