# Behavioral analysis tools

## Edward J. Stronge - ejstronge@gmail.com

A group of utilities for analyzing behavioral neuroscience time series data.
The data are encoded in binary or base-3 files; this suite parses these
files and produces summaries of the behavior of each animal under observation.

These utilities are written in R and Bash shell scripts.


## Installation

To install the R dependencies, run the `make install_packages` target.

Afterwards, run the `make install` target - this will add a Makefile to each subdirectory
of information in the `data` directory.

If a folder has observation data that require special processing, add its
name to the `special_data` array in `install_makefiles.sh`.


## Usage

After installation, simply run `make`. This will summarize the observations
in each folder, creating a `ggplot2` graphic of each animals' behavior
and a spreadsheet summarizing the following data for each observation file:

- number of times an animal moves from one compartment to another
- proportion of time spent in either the light or dark compartment
- number of frames analyzed

To re-run analysis, run `make clean` and simply `make install` again.

### Using the core analysis script

To augment the batch processing script or to use it on an individual directory
of data, use the following parameters. This documentation was generated using
the R package `optparse`.

```
$ axolotl_analysis_script.R --help
Usage: axolotl_analysis_script.R [options]

Options:
        --start=START
                Beginning of analysis period, in SECONDS. Use 'START' to begin
                at the first frame [default START]

        --end=END
                End of analysis period, in SECONDS. Use 'END' to end at the
                last frame [default END]

        --duration=DURATION
                Only analyze DURATION of each file, measured in SECONDS. If
                DURATION is negative start at the end of the file. DURATION
                takes precedence over START and END

        -d DIRECTORY, --directory=DIRECTORY
                Directory where output files will be saved.

        --light=LIGHT
                Value indicating axolotls are in the light. [default 0]

        --dark=DARK
                Value indicating axolotls are in the dark. [default 1]

        --shock=SHOCK
                Value indicating axolotls are being shocked. [default 2]

        -h, --help
                Show this help message and exit
```


## Testing

This uses the `testthat` package by Hadley Wickham. To run a test, execute.

```sh
tests/run_tests tests/
```

Additional tests can also be added to the `tests/` folder.