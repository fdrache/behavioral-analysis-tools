
DATA_DIRECTORIES := $(addprefix "data/",$(shell ls data/))
SUBCLEAN := $(addsuffix .clean,$(addprefix "data/",$(shell ls data/)))
SUBPURGE := $(addsuffix .purge,$(addprefix "data/",$(shell ls data/)))

START = 5
END = 45
export START END

.PHONY: all clean $(SUBCLEAN) install

all: $(DATA_DIRECTORIES)

$(DATA_DIRECTORIES):
	@$(MAKE) -C "$@"

clean: $(SUBCLEAN)

purge: $(SUBPURGE)

install_packages:
	@mkdir -p packages
	Rscript -e "install.packages(c('optparse', 'ggplot2', 'testthat'), repos='http://watson.nci.nih.gov/cran_mirror/')"

install:
	./install_makefiles.sh

$(SUBCLEAN): %.clean:
	@$(MAKE) -C "$*" clean

$(SUBPURGE): %.purge:
	@$(MAKE) -C "$*" purge
